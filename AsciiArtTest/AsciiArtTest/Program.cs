﻿using System;
using System.Text;

namespace AsciiArtTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;

            Console.Write('╠');
            Console.WriteLine('☺');
            Console.WriteLine('☻');
            Console.WriteLine('♥');
            Console.WriteLine("♦♣♠•◘○◙");

            Console.ReadLine();
        }
    }
}
