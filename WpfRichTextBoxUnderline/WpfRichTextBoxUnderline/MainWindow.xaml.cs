﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfRichTextBoxUnderline
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private TextDecorationCollection GetTextDecorations()
        {
            TextDecorationCollection textDecorations = null;
            var caret = RichText.CaretPosition;
            Paragraph paragraph = RichText.Document.Blocks.FirstOrDefault(x => x.ContentStart.CompareTo(caret) == -1 && x.ContentEnd.CompareTo(caret) == 1) as Paragraph;

            if (paragraph != null)
            {
                Inline inline = paragraph.Inlines.FirstOrDefault(x => x.ContentStart.CompareTo(caret) == -1 && x.ContentEnd.CompareTo(caret) == 1) as Inline;
                if (inline != null)
                {
                    textDecorations = inline.TextDecorations;
                    //this.UnderlineButton.IsChecked = (textDecorations == DependencyProperty.UnsetValue) ? false : textDecorations != null && textDecorations.Contains(TextDecorations.Underline[0]);
                }
            }
            return textDecorations;
        }

        private void SetSelectionUnderline(bool isUnderlined)
        {
            var selection = this.RichText.Selection;

            if (selection != null)
            {
                TextDecorationCollection textDecorations = this.GetTextDecorations();

                var newTextDecorations = new TextDecorationCollection();
                if (isUnderlined)
                    newTextDecorations.Add(TextDecorations.Underline);

                selection.ApplyPropertyValue(Inline.TextDecorationsProperty, newTextDecorations);
            }
        }

        private void RichTextBox_SelectionChanged(object sender, RoutedEventArgs e)
        {
            if (this.RichText.Selection != null)
            {
                object currentValue = this.RichText.Selection.GetPropertyValue(Inline.TextDecorationsProperty);
                this.UnderlineButton.IsChecked = (currentValue == DependencyProperty.UnsetValue) ? false : currentValue != null && (currentValue as TextDecorationCollection).SequenceEqual(TextDecorations.Underline);
            }
        }

        public Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        private void SaveAndReloadButton_Click(object sender, RoutedEventArgs e)
        {
            var range = new TextRange(this.RichText.Document.ContentStart, this.RichText.Document.ContentEnd);
            using (var memoryStream = new MemoryStream())
            {
                range.Save(memoryStream, DataFormats.XamlPackage);
                memoryStream.Position = 0;

                // load

                range = new TextRange(this.RichText.Document.ContentStart, this.RichText.Document.ContentEnd);
                range.Load(memoryStream, DataFormats.XamlPackage);
            }
        }

        private void UnderlineButton_Checked(object sender, RoutedEventArgs e)
        {
            this.SetSelectionUnderline(true);
        }

        private void UnderlineButton_Unchecked(object sender, RoutedEventArgs e)
        {
            this.SetSelectionUnderline(false);
        }
    }
}
