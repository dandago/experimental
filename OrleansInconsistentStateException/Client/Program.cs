﻿using Interfaces;
using Orleans;
using Orleans.Runtime;
using Orleans.Runtime.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Client";

            var config = ClientConfiguration.LoadFromFile("ClientConfiguration.xml");

            while (true)
            {
                try
                {
                    GrainClient.Initialize(config);
                    Console.WriteLine("Connected to silo!");

                    for (int i = 0; i < 500; i++)
                        SayHelloAsync(i);

                    Console.ReadLine();
                    break;
                }
                catch (SiloUnavailableException)
                {
                    Console.WriteLine("Silo not available! Retrying in 2 seconds.");
                    Thread.Sleep(2000);
                }
            }
        }

        static async void SayHelloAsync(int i)
        {
            try
            {
                var friend = GrainClient.GrainFactory.GetGrain<IPerson>(i);
                await friend.SayHelloAsync("Hello!"); // <<<<--------  !!!!!
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}
