﻿using Interfaces;
using Orleans;
using Orleans.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grains
{
    public class GrainState
    {
        public string Str { get; set; }
    }

    [StorageProvider(ProviderName = "PersistenceStore")]
    public class PersonGrain : Grain<GrainState>, IPerson
    {
        public async Task SayHelloAsync(string message)
        {
            long name = this.GetPrimaryKeyLong();
            Console.WriteLine($"{name} says: {message}");

            State.Str = "Hello";
            await WriteStateAsync();
        }
    }
}
