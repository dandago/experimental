﻿using Interfaces;
using Orleans;
using Orleans.Runtime;
using Orleans.Runtime.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Client";

            Run();

            Console.ReadLine();
        }

        static async void Run()
        {
            var config = ClientConfiguration.LoadFromFile("ClientConfiguration.xml");

            while (true)
            {
                try
                {
                    using (var client = new ClientBuilder()
                        .LoadConfiguration().Build())
                    {
                        await client.Connect();

                        Console.WriteLine("Connected to silo!");

                        var dog = client.GetGrain<IDog>("Fido");
                        await dog.BarkAsync();

                        await client.Close();
                    }
                }
                catch (SiloUnavailableException)
                {
                    Console.WriteLine("Silo not available! Retrying in 3 seconds.");
                    Thread.Sleep(3000);
                }
            }
        }
    }
}
