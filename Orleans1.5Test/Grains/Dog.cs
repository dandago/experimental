﻿using Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grains
{
    public class Dog : IDog
    {
        public Task BarkAsync()
        {
            Console.WriteLine("Woof!");
            return Task.CompletedTask;
        }
    }
}
