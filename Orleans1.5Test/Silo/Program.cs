﻿using Orleans.Runtime.Host;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Silo
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Silo";

            try
            {
                using (var silo = new SiloHost(Dns.GetHostName()))
                {
                    silo.LoadOrleansConfig();

                    Console.WriteLine("Starting silo...");
                    silo.InitializeOrleansSilo();
                    bool started = silo.StartOrleansSilo(catchExceptions: false);

                    if (started)
                    {
                        Console.WriteLine("Silo started");

                        Console.WriteLine("Press ENTER to stop the silo...");
                        Console.ReadLine();
                        
                        Console.WriteLine("Stopping silo...");
                        silo.StopOrleansSilo();
                        Console.WriteLine("Silo stopped");
                        silo.UnInitializeOrleansSilo();
                    }
                    else
                    {
                        Console.WriteLine("Failed to start!");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            Console.WriteLine("Press ENTER to exit...");
            Console.ReadLine();
        }
    }
}
