﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedisClientStress
{
    class Program
    {
        private static Lazy<ConfigurationOptions> configOptions = new Lazy<ConfigurationOptions>(() =>
            {
                var configOptions = new ConfigurationOptions();
                configOptions.EndPoints.Add("localhost:6379");
                configOptions.ClientName = "SafeRedisConnection";
                configOptions.ConnectTimeout = 100000;
                configOptions.SyncTimeout = 100000;
                configOptions.AbortOnConnectFail = false;
                return configOptions;
            });

        private static Lazy<ConnectionMultiplexer> conn = new Lazy<ConnectionMultiplexer>(
                () => ConnectionMultiplexer.Connect(configOptions.Value));

        private static ConnectionMultiplexer SafeConn
        {
            get
            {
                return conn.Value;
            }
        }

        static void Main(string[] args)
        {
            Run();
            Console.ReadLine();
        }

        static async void Run()
        {
            var db = SafeConn.GetDatabase(0);

            const string key = "KEY";
            string value = new string('a', 40 * 1000 * 1000);
            await db.StringSetAsync(key, value);

            for (int iterationCount = 1; iterationCount < 200; iterationCount++)
            {
                var list = new List<Task>();
                for (int i = 0; i < iterationCount; i++)
                {
                    var task = db.StringGetAsync(key);
                    list.Add(task);
                }
                await Task.WhenAll(list);
                Console.WriteLine("Test with {0} iterations completed", iterationCount);
            }
        }
    }
}
